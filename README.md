Tarea Examen

Reyes Granados Naomi Itzel.

Escamilla Monrroy Miguel Angel.

Verdugo Daniel.

Macias Gómez Jorge.

Demonio:
En informática, vamos a llamar un demonio a un proceso que se ejecuta en segundo plano, es decir que no es controlado por el usuario directamente. Este se ejecuta de forma continua y aunque se intente cerrar o matar , este continuara su ejecución o se reiniciará automáticamente.

Sokets:
Concepto abstracto por el cual procesos pueden intercambiar cualquier flujo de datos, en otras palabras, es un canal para intercambiar mensajes entre dos procesos. Existen dos tipos:

    • TCP: Son orientados a conexión, es decir, necesitas conexión para poder mandar mensajes.
    • UDP: No son orientados a conexión, es decir, no necesitas conexión para poder mandar mensajes, por esto no se segura que lleguen los mensajes a su destino.

Protocolo Syslog:
Es un estándar en el envió de mensajes de registro que se trata de un ordenador servidor ejecutando el servidor de syslog. El cliente envía un pequeño mensaje de texto (de menos de 1024 bytes), este debe de tener los siguientes datos Prioridad, Cabecera (tiempo y ordenador) y Texto.

Implementación.

Para la implementación del demonio tenemos dos código el primero inicio que deberá estar guardada en /etc/init.d; este código nos ayudara a manejar el inicio y el final de la ejecución del demonio. Ejecutamos lo siguiente en la terminal:

    • chmod 755 /etc/init.d/demoniosyslog
    • chown root:root /etc/init.d/demoniosyslog
Estos para que se pueda ejecutar como un comando de prioridad mayoritaria. 
Ahora debemos tener en cuenta que existen diferentes niveles de arranque, los mencionaremos a continuación:

    • 0 detener o apagar el sistema.
    • 1 Modo monosuario, para mantenimiento.
    • 2 Modo multiusuario.
    • 3 Modo multiusuario completo.
    • 4 --------
    • 5 Modo multiusuario completo con inicio gráfico.
    • 6 Modo de reinicio.
Nos importa que nuestro demonio corra en los niveles 2,3,4 y 5; y se detenga en los niveles 0,1 y 6. Por esto hay que ejecutar lo siguiente en la terminal:

    • ln -s  /etc/init.d/inicio /etc/rc2.d/S76demoniosyslog
    • ln -s  /etc/init.d/inicio /etc/rc3.d/S76demoniosyslog
    • ln -s  /etc/init.d/inicio /etc/rc4.d/S76demoniosyslog
    • ln -s  /etc/init.d/inicio /etc/rc5.d/S76demoniosyslog
    • ln -s  /etc/init.d/inicio /etc/rc1.d/K76demoniosyslog
    • ln -s  /etc/init.d/inicio /etc/rc0.d/K76demoniosyslog
    • ln -s  /etc/init.d/inicio /etc/rc6.d/K76demoniosyslog
      
Ahora vamos al codigo del demonio en si, se encuentra en demoniosyslog.c, a grandes rasgos lo que hacemos para crear nuestro demonio es: “Crear un hilo de ejecución con fork y la aplicación principal se saldrá. Esto provoca que el hilo de ejecución se quede huérfano. Después se ejecuta umask para no heredar los permisos de los ficheros del proceso padre. A continuación se ejecuta setsid para que el proceso huérfano tenga su propio SID y sea un proceso independiente para el sistema operativo. Se cambia el directorio de ejecución para apuntar a la raíz con un chdir ya que es el único directorio seguro que existe en linux.”.

Dentro de este demonio estará nuestro socket de red y el protocolo, que lo que hace es tomar los mensajes que le vallan llegando y guardarlos en un archivo. Más especifico lo que va a realizar es: 

    • Creamos el socket de red (en nuestro caso de tipo TCP).
    • Avisamos al sistema operativo de su creación (bind()).
    • Avisamos al sistema operativo que atienda la conexión (listen()).
    • Aceptamos la conexión con el cliente (accept()).
    • Tomamos los datos y los escribimos en los archivos.
    • Cerramos nuestro socket.
Deberíamos tener dos sockets mas conectados a /dev /kmsg y a /dev /log; pero aunque sabemos que el socket tenia que ser de tipo UNIX no supimos como vincularlo a estos ya existentes.


