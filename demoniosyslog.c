#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>
#include <signal.h>
#include <netdb.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h>
#include <time.h>

#define FICHERO_PID "/var/run/demoniosyslog.pid"
//Tamaño maximo del mensaje
#define MAX 3840
//Puerto para el socket de red de tipo TCP        
#define PORT 514
#define SA struct sockaddr 
volatile char estado = 0;
 
/* Función llamada cuando se recibe una señal SIGTERM */
void adios( int signum )
{
  estado = 1;
}
 
int main( void )
{
  struct stat st;
  FILE *fichero_pid;
  FILE *fichero_prueba;
  pid_t pid;
  pid_t sid;
  int sockfd, connfd, len; 
  struct sockaddr_in servaddr, cli; 
  char buff[MAX]; 
  int n;
  //Variable del archivo generico donde vamos a guardar los mensajes
  FILE *archivo;
  
  time_t t;
  struct tm *tm;
  char fechayhora[100];
  
  /* Activamos el manejo de mensajes al demonio de syslogd */
  openlog( "demoniosyslog", LOG_CONS | LOG_PID, LOG_DAEMON );
 
  /* Comprobamos si existe le fichero PID */
  if( stat( FICHERO_PID, &st ) == 0 )
    {
      syslog( LOG_ERR, "Ya existe un proceso similar cargado" );
      exit( EXIT_FAILURE );
    }
 
  /* Creamos el fichero PID */
  fichero_pid = fopen( FICHERO_PID, "w" );
  if( fichero_pid == NULL )
    {
      syslog( LOG_ERR, "No se pudo crear el fichero PID" );
      exit( EXIT_FAILURE );
    }
 
  /* Creamos un hilo de ejecucion */
  pid = fork();
    
  /* Si no se pudo crear el hilo */
  if( pid < 0 )
    {
      syslog( LOG_ERR, "No se pudo crear el proceso hijo" );
      fclose( fichero_pid );
      exit( EXIT_FAILURE );
    }
  /* Si se pudo crear el hilo */
  else if( pid > 0 )
    {
      /* Escribimos en el fichero PID el identificador del proceso */
      fprintf( fichero_pid , "%d\n", pid );
      fclose( fichero_pid );
      exit( EXIT_SUCCESS );
    }
    
  /* Se evita el heredar la máscara de ficheros */
  umask( 0 );
 
  /* Convertimos el proceso hijo a demonio */
  sid = setsid();
 
  /* Si no se pudo convertir en demonio */
  if( sid < 0 )
    {
      syslog( LOG_ERR, "No se pudo crear el demonio" );
      unlink( FICHERO_PID );
      exit( EXIT_FAILURE );
    }
 
  /* Cambiamos al directorio raiz */
  if( chdir( "/" ) < 0 )
    {
      syslog( LOG_ERR, "No se pudo cambiar el directorio" );
      unlink( FICHERO_PID );
      exit( EXIT_FAILURE );
    }
 
  /* Cerramos los descriptores de archivo que no usaremos */
  close( STDIN_FILENO );
  close( STDOUT_FILENO );
  close( STDERR_FILENO );
 
 
  /* Registramos la función que recibe la señal SIGTERM */
  signal( SIGTERM, adios );
 
  /* Bucle principal que se ejecuta hasta que se reciba un SIGTERM (ejecución del comando kill) */
  while ( estado == 0 )
    {
      //creacion y verificacion del socket
      sockfd = socket(AF_INET, SOCK_STREAM, 0);
      
      if (sockfd == -1) { 
        printf("socket creation failed...\n"); 
        exit(0); 
      } 
      else
        printf("Socket successfully created..\n"); 
      bzero(&servaddr, sizeof(servaddr)); 

      // Familia Internet/de red
      servaddr.sin_family = AF_INET;
      //coloca nuestra direccion IP automaticamente
      servaddr.sin_addr.s_addr = htonl(INADDR_ANY); 
      servaddr.sin_port = htons(PORT); 

    
      if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0) { 
        printf("socket bind failed...\n"); 
        exit(0); 
      } 
      else
        printf("Socket successfully binded..\n"); 
    
      // Estamos listos para escuchar por nuestro socket
      if ((listen(sockfd, 5)) != 0) { 
        printf("Listen failed...\n"); 
        exit(0); 
      } 
      else
        printf("Server listening..\n"); 
      len = sizeof(cli); 

      // Aceptamos los datos del cliente y verificamos
      connfd = accept(sockfd, (SA*)&cli, &len); 
      if (connfd < 0) { 
        printf("server acccept failed...\n"); 
        exit(0); 
      } 
      else
        printf("server acccept the client...\n"); 


      // infinite loop for chat 
      for (;;) { 
        bzero(buff, MAX); 
        
        // leemos el mensaje del uusario y lo guardamos en el buffer
        read(sockfd, buff, sizeof(buff));

        //Decidimos en que archivo vamos a guardar
        if (strncmp("Kernel", buff, 6) == 0) { 
          archivo = fopen("/tmp/Kernel.txt", "a+");
        }
        else
          archivo = fopen("/tmp/demoniosyslog.txt", "a+");
  
        // Si no podemos abrir el archivo, terminamos el programa
        if(archivo == NULL){
          printf("No se pudo abrir el archivo... \n"); return -1;
        }

        t=time(NULL);
        tm=localtime(&t);
        strftime(fechayhora, 100, "%d/%m/%Y", tm);
          
        //Guardamos fecha y hora
        fputs(archivo, fechayhora);
        //Guardamos el mansaje en un archivo
        fputs(archivo, strcat(inet_ntoa(cli.sin_addr),buff));
         

        //Cerramos el archivo.
        fclose(archivo);
          
        // if msg contains "Exit" then server exit and chat ended. 
        if (strncmp("exit", buff, 4) == 0) { 
          printf("Server Exit...\n"); 
          break;
        }
      }         

      // After chatting close the socket 
      close(sockfd); 
      sleep( 1 );
    }
 
  /* Borramos el fichero PID */
  unlink( FICHERO_PID );
  /* Cerramos el log */
  closelog();
}
